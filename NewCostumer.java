
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class NewCostumer extends JFrame {
	
		private JButton jButton1;
	    private JLabel welcome;
	    private JLabel country;
	    private JLabel balance;
	    private JLabel lastName;
	    private JLabel firstName;
	    private JLabel sex;
	    private JLabel dob;
	    private JLabel socNo;
	    private JLabel nationality;
	    private JLabel strAdd;
	    private JLabel pCode;
	    private JLabel city;
	    private JMenu jMenu1;
	    private JMenu jMenu2;
	    private JMenuBar jMenuBar1;
	    private JPanel costumerPanel;
	    private JRadioButton jRadioButton1;
	    private JRadioButton jRadioButton2;
	    private JTextField jTextField1;
	    private JTextField jTextField10;
	    private JTextField jTextField2;
	    private JTextField jTextField3;
	    private JTextField jTextField4;
	    private JTextField jTextField5;
	    private JTextField jTextField6;
	    private JTextField jTextField7;
	    private JTextField jTextField8;
	    private JTextField jTextField9;
	    String sexy = "M";
	    Costumer ncostumer;
    public NewCostumer() {
     
        costumerPanel = new JPanel();
        welcome = new JLabel();
        firstName = new JLabel();
        sex = new JLabel();
        dob = new JLabel();
        socNo = new JLabel();
        nationality = new JLabel();
        strAdd = new JLabel();
        pCode = new JLabel();
        city = new JLabel();
        country = new JLabel();
        balance = new JLabel();
        lastName = new JLabel();
        jButton1 = new JButton();
        jTextField1 = new JTextField();
        jTextField2 = new JTextField();
        jTextField3 = new JTextField();
        jTextField4 = new JTextField();
        jTextField5 = new JTextField();
        jTextField6 = new JTextField();
        jTextField7 = new JTextField();
        jTextField8 = new JTextField();
        jTextField9 = new JTextField();
        jRadioButton1 = new JRadioButton("Male", true);
        jRadioButton2 = new JRadioButton("Female");
        jTextField10 = new JTextField();
        jMenuBar1 = new JMenuBar();
        jMenu1 = new JMenu();
        jMenu2 = new JMenu();
         
        setTitle("New Customer Wizard");
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        welcome.setFont(new java.awt.Font("Times New Roman", 1, 18));
        welcome.setText("New Customer Detail");

        firstName.setFont(new java.awt.Font("Tahoma", 0, 12));
        firstName.setText("Last Name");

        sex.setFont(new java.awt.Font("Tahoma", 0, 12));
        sex.setText("First Name");

        dob.setFont(new java.awt.Font("Tahoma", 0, 12));
        dob.setText("Sex");

        socNo.setFont(new java.awt.Font("Tahoma", 0, 12));
        socNo.setText("Date of birth (dd/mm/yyyy)");

        nationality.setFont(new java.awt.Font("Tahoma", 0, 12));
        nationality.setText("Social security number");

        strAdd.setFont(new java.awt.Font("Tahoma", 0, 12));
        strAdd.setText("Nationality");

        pCode.setFont(new java.awt.Font("Tahoma", 0, 12));
        pCode.setText("Street address");

        city.setFont(new java.awt.Font("Tahoma", 0, 12));
        city.setText("Postal code");

        country.setFont(new java.awt.Font("Tahoma", 0, 12));
        country.setText("City");

        balance.setFont(new java.awt.Font("Tahoma", 0, 12));
        balance.setText("Country");

        lastName.setFont(new java.awt.Font("Tahoma", 0, 12));
        lastName.setText("Startup Balance");

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jButton1.setText("Create Account");
        jButton1.addActionListener(new CreateAccountListener());

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField3.setFont(new java.awt.Font("Tahoma", 0, 12));
        
        

        jTextField4.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField5.setFont(new java.awt.Font("Tahoma", 0, 12));
        

        jTextField6.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField7.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField8.setFont(new java.awt.Font("Tahoma", 0, 12));

        jTextField9.setFont(new java.awt.Font("Tahoma", 0, 12));

        
        ButtonGroup group =  new ButtonGroup();
        group.add(jRadioButton1);
        group.add(jRadioButton2);
        
        jRadioButton1.addActionListener(new RadioButtonListener());
        jRadioButton2.addActionListener(new RadioButtonListener());
        
        jTextField10.setFont(new java.awt.Font("Tahoma", 0, 12));

        javax.swing.GroupLayout costumerPanelLayout = new javax.swing.GroupLayout(costumerPanel);
        costumerPanel.setLayout(costumerPanelLayout);
        costumerPanelLayout.setHorizontalGroup(
            costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(costumerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(welcome)
                    .addGroup(costumerPanelLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(costumerPanelLayout.createSequentialGroup()
                                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pCode)
                                    .addGroup(costumerPanelLayout.createSequentialGroup()
                                        .addComponent(dob)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jRadioButton1))
                                    .addComponent(sex)
                                    .addComponent(firstName)
                                    .addComponent(socNo)
                                    .addComponent(nationality)
                                    .addComponent(strAdd))
                                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(costumerPanelLayout.createSequentialGroup()
                                        .addGap(49, 49, 49)
                                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jRadioButton2)
                                            .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jTextField4, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextField3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE))
                                    .addGroup(costumerPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jTextField2, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE))
                                    .addGroup(costumerPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
                                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, costumerPanelLayout.createSequentialGroup()
                                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(costumerPanelLayout.createSequentialGroup()
                                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(city)
                                            .addComponent(balance)
                                            .addComponent(lastName))
                                        .addGap(31, 31, 31)
                                        .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField8, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(country)))
                                .addGap(18, 18, 18)
                                .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(30, 30, 30))
        );
        costumerPanelLayout.setVerticalGroup(
            costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(costumerPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(welcome)
                .addGap(24, 24, 24)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(firstName)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sex)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton2)
                    .addComponent(dob)
                    .addComponent(jRadioButton1))
                .addGap(15, 15, 15)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(socNo))
                .addGap(21, 21, 21)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nationality))
                .addGap(18, 18, 18)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(strAdd))
                .addGap(18, 18, 18)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pCode)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(city)
                    .addComponent(country))
                .addGap(18, 18, 18)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(balance))
                .addGap(22, 22, 22)
                .addGroup(costumerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lastName, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(jButton1)
                .addContainerGap(54, Short.MAX_VALUE))
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(costumerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(costumerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }
    
    public class RadioButtonListener implements ActionListener{
    	public void actionPerformed(ActionEvent e){
    		if(e.getSource() == jRadioButton1){
    			sexy = "M";
    			}
    		else if(e.getSource() == jRadioButton2){
    			sexy = "F";
    		}
    		else
    			 sexy = "M";
    		 		
    	}
    }
    
    public class CreateAccountListener implements ActionListener{
    		public void actionPerformed(ActionEvent e){
    			String nlastname = jTextField1.getText(); 
    			String nfirstname = jTextField2.getText();
    			String nsex = sexy; 
    			String ndob = jTextField3.getText(); 
    			String nsocno = jTextField4.getText(); 
    			String nnationality = jTextField5.getText(); 
    			String nstradd = jTextField6.getText(); 
    			String npcode = jTextField7.getText(); 
    			String ncity = jTextField8.getText(); 
    			String ncountry = jTextField9.getText(); 
    			double nbalance = Double.parseDouble(jTextField10.getText());
    			ncostumer = new Costumer(nlastname, nfirstname, nsex, ndob, nsocno, nnationality, nstradd, npcode, ncity, ncountry,nbalance);
    			ncostumer.createCostumer();
    		}
    		
    	
    }
    
    public static void main(String [] args){
		NewCostumer nc = new NewCostumer();
	}


}
