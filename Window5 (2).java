import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/*This window will provide the customer detail
  and will be saved in data base for trail.
 */

public class Window5 extends JFrame
{
   private JLabel firstName; 	// First name label
   private JLabel lastName;	//last name label
   private JLabel socialSecurity;  // social security label
   private JTextField fn; // Text field for firt name
   private JTextField ln;  // Text field for last name
   private JTextField ss;  //Text field for Social Security number
   private JPanel fpanel;         // first name panel
   private JPanel lpanel;         // last name panel
   private JPanel spanel;		// last name panel
   private JButton createButton;		// Create Account Button
  

   /**
      Constructor
   */

   public Window5()
   {
      // Set the title.
      setTitle("Windows no. 5");

      // Specify an action for the close button.
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      // Create the message labels.
      firstName = new JLabel("First Name: ");
      lastName = new JLabel("Last Name: ");
      socialSecurity = new JLabel("Social Security: ");
      
      // Create the Enter Button
      createButton = new JButton("Create Account");
	   
 	 //Add an action listener to the button.
 	   createButton.addActionListener(new CreateButtonListener());
      
      // Create the read-only text fields.
      fn = new JTextField(null, 30);
      fn.setEditable(true);
      ln = new JTextField(null, 30);
      ln.setEditable(true);
      ss = new JTextField(null, 30);
      ss.setEditable(true);

      // Create panels and place the components in them.
      fpanel = new JPanel();
      fpanel.add(firstName);
      fpanel.add(fn);
      lpanel = new JPanel();
      lpanel.add(lastName);
      lpanel.add(ln);
      spanel= new JPanel();
      spanel.add(socialSecurity);
      spanel.add(ss);
      
      

      // Create a GridLayout manager.
      setLayout(new GridLayout(5, 3));

      // Add the panels to the content pane.
      add(fpanel);
      add(lpanel);
      add(spanel);
      add(createButton);
      
      // Pack and display the frame.
      pack();
      setVisible(true);
   }
   
      private class CreateButtonListener implements ActionListener{
	   
	   public void actionPerformed(ActionEvent e){
		   
	   }
   }
     
   
   public static void main(String[] args)
   {
      Window5 w5 = new Window5();
   }
}